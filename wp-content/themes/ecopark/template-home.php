<?php
/* Template Name: Home Template */
get_header();
?>
<main id="main">
    <?php get_template_part('blocks/home', 'banner') ?>
</main>
<?php get_footer(); ?>