<!DOCTYPE html>
<!--[if IE 8 ]>
<html class="ie ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html <?php language_attributes(); ?>> <!--<![endif]-->
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <?php wp_head(); ?>
    <link href="<?php echo THEME_URL ?>/assets/fonts/stylesheet.css" rel="stylesheet">
    <!-- <link href="<?php echo THEME_URL ?>/assets/css/bootstrap.min.css" rel="stylesheet"> -->
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css"/>
    <link href="<?php echo THEME_URL ?>/assets/css/owl.carousel.css" rel="stylesheet">
    <link href="<?php echo THEME_URL ?>/assets/css/owl.theme.default.min.css" rel="stylesheet">
    <link href="<?php echo THEME_URL ?>/assets/css/custom.css?v=<?php echo time() ?>" rel="stylesheet">
    <link href="<?php echo THEME_URL ?>/assets/css/screen.css?v=<?php echo time() ?>" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src='<?php echo THEME_URL ?>/assets/js/html5shiv.min.js'></script>
    <script src='<?php echo THEME_URL ?>/assets/js/respond.min.js'></script>
    <![endif]-->

    <style type="text/css">
        body{
            margin:0px;
        }
    </style>
</head>

<body <?php body_class(); ?>>

<?php
/**
 * Add Google Analytics tracking script
 *
 */
do_action('cus_add_google_tracking_code');
?>

<!-- wrapper -->
<div id="wrapper">
    <!-- header -->
    <header id="header">
        <div class="header__innner">
            <?php echo do_shortcode('[elementor-template id="114"]'); ?>
        </div>
    </header>
    <!-- /header -->

