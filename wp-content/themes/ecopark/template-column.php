<?php
/* Template Name: Column page Template */
get_header();
?>

   <main class="page-column" id="main">
   	<div class="banner">
            <?php
            $banner  = get_field('page_banner');
            if (!empty($banner)):
                ?>
                <img src="<?php echo $banner; ?>" alt="<?php the_title(); ?>">
            <?php endif; ?>
      </div>
   	<div class="container">
   		<div class="content-row">
   			<div class="content-desc">
   				<?php the_content(); ?>
   			</div>
   			<div class="content-column">
   				<?php  get_template_part('blocks/sidebar_column'); ?>
   			</div>
   		</div>
   	   </div>
   </main>

<?php get_footer(); ?>