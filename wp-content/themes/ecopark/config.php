<?php

/* Register global - DO NOT EDIT HERE */


/* FRONT-END START EDITING HERE */

/** Content width (px) - This is the width of article content div, oembed size and max image width... */
$dev_content_w = 900;

/** Script version - Modify it if you got cached css & js */
$dev_script_version = '1.0';

/** Fonts script */
$dev_font_styles = array(/*array(
		'name'    => 'font-name',
		'src'       => '',
	),*/

);

/** JS scripts - Notice: Do not add jquery.js, WP automatically handle it before these files  */
$dev_js_scripts = array(
    /*array(
        'name'    => 'name-of-js',
        'src'       => ''
    ),*/
//    array(
//        'name' => 'fe-jquery-migrate',
//        'src' => 'https://code.jquery.com/jquery-migrate-3.3.0.js'
//    ),

);

/** include styles */
$dev_styles = array(


    array(
        'name' => 'fe-screen-style',
        'src' => 'assets/css/style.css',
        'screen' => 'all'
    ),

);

/** Editor style source - wp-editor class */
$dev_editor_style_src = 'assets/css/editor.min.css';


/* BACK-END START EDITING HERE */

/** post thumbnail sizes (px) - this is the thumbnail image of blog posts */
$dev_post_thumbnail_size = array(
    'width' => 400,
    'height' => 300,
    'crop' => true
);

/** Additional image sizes (px) */
$dev_add_sizes = array(/*'name_of_size' => array( 'width' => 100, 'height' => 100, 'crop' => true ),*/
);

/** Copyright name & url */
$dev_copyright_name = 'VNITWEB';
$dev_copyright_url = 'https://vnitweb.com';

/** Theme support */

$dev_post_formats = array(
    'aside',
    //'image',
    //'video',
    //'quote',
    //'link'
);
global $dev_textdomain;
$dev_textdomain = 'dev_theme';

/** Navigation menu */
$dev_nav_menus = array(
    'main' => __('Main Menu', $dev_textdomain),
    'footer' => __( 'Footer Menu', $dev_textdomain ),
);

/** Sidebar */
$dev_sidebars = array(/*array(
		'name'        => __( 'Name of Sidebar', $dev_textdomain ),
		'id'          => 'id-of-sidebar',
		'description' => ''
	),*/
);

/** Widgets */
$dev_widgets = array(/*array(
		'name' => 'SB_Name_Of_Widget_Class',
		'path' => 'inc/widgets/widget_class_file.php',
	),*/
);

/* OK, that's enough, Stop editing */