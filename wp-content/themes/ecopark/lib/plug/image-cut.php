<?php

/**
 * Class Cus_Image_Cut
 *
 * @param $image : image url or id
 * @param $size : size (width, heigth)
 * @param $is_id : true if insert image id, false if insert image url
 * @param null $acf_option : acf option fields (image default option, ...)
 */
class Cus_Image_Cut {

	public $image;
	public $size;
	public $is_id;
	public $acf_option;
	public static $instance;

	public static function getInstance()
	{
		if( !static::$instance ) {
			static::$instance = new static;
		}
		return static::$instance;
	}

	public function __construct() {
		//Hooks
		add_filter( 'image_resize_dimensions', array( $this, 'thumbnail_upscale' ), 10, 6 );
		add_filter( 'image_downsize', array( $this, 'image_downsize' ), 10, 3 );
	}

	public function set_data( $image, array $size, $is_id, $acf_option ){
		//Variables
		$this->image         = $image;
		$this->size       = $size;
		$this->is_id      = $is_id;
		$this->acf_option = $acf_option;
	}

	/**
	 * Fit resize with small images
	 */

	function thumbnail_upscale( $default, $orig_w, $orig_h, $new_w, $new_h, $crop ) {
		if ( ! $crop ) {
			return null;
		} // let the wordpress default function handle this

		$aspect_ratio = $orig_w / $orig_h;
		$size_ratio   = max( $new_w / $orig_w, $new_h / $orig_h );

		$crop_w = round( $new_w / $size_ratio );
		$crop_h = round( $new_h / $size_ratio );

		$s_x = floor( ( $orig_w - $crop_w ) / 2 );
		$s_y = floor( ( $orig_h - $crop_h ) / 2 );

		return array( 0, 0, (int) $s_x, (int) $s_y, (int) $new_w, (int) $new_h, (int) $crop_w, (int) $crop_h );
	}


	function get_image_sizes( $size = '' ) {
		global $_wp_additional_image_sizes;
		$sizes                        = array();
		$get_intermediate_image_sizes = get_intermediate_image_sizes();
		// Create the full array with sizes and crop info
		foreach ( $get_intermediate_image_sizes as $_size ) {
			if ( in_array( $_size, array( 'thumbnail', 'medium', 'large' ) ) ) {
				$sizes[ $_size ]['width']  = get_option( $_size . '_size_w' );
				$sizes[ $_size ]['height'] = get_option( $_size . '_size_h' );
				$sizes[ $_size ]['crop']   = (bool) get_option( $_size . '_crop' );
			} elseif ( isset( $_wp_additional_image_sizes[ $_size ] ) ) {
				$sizes[ $_size ] = array(
					'width'  => $_wp_additional_image_sizes[ $_size ]['width'],
					'height' => $_wp_additional_image_sizes[ $_size ]['height'],
					'crop'   => $_wp_additional_image_sizes[ $_size ]['crop']
				);
			}
		}
		// Get only 1 size if found
		if ( $size ) {
			if ( isset( $sizes[ $size ] ) ) {
				return $sizes[ $size ];
			} else {
				return false;
			}
		}

		return $sizes;
	}

	/**
	 * Cut Image
	 */
	function image_downsize( $downsize = true, $image, $size, $crop = true ) {
		$meta = wp_get_attachment_metadata( $image );
		if ( empty( $meta ) ) {
			return false;
		}
		$sizes = $this->get_image_sizes();

		if ( is_array( $size ) && sizeof( $size ) == 2 ) {
			list( $width, $height ) = $size;
			$size = $width . 'x' . $height;

			foreach ( $sizes as $size_name => $size_atts ) {
				if ( $width == $size_atts['width'] && $height == $size_atts['height'] ) {
					$size = $size_name;
				}
			}
		} elseif ( array_key_exists( $size, $sizes ) ) {
			extract( $sizes[ $size ] );
		} else {
			return false;
		}

		if ( array_key_exists( $size, $meta['sizes'] ) && $width == $meta['sizes'][ $size ]['width'] && $height == $meta['sizes'][ $size ]['height'] ) {
			return false;
		}
		$intermediate = image_make_intermediate_size( get_attached_file( $image ), $width, $height, $crop );

		if ( ! is_array( $intermediate ) ) {
			return false;
		}

		$meta['sizes'][ $size ] = $intermediate;
		wp_update_attachment_metadata( $image, $meta );

		list( $width, $height ) = image_constrain_size_for_editor( $intermediate['width'], $intermediate['height'], $size );

		$original_url      = wp_get_attachment_url( $image );
		$original_basename = wp_basename( $original_url );
		$img_url           = str_replace( $original_basename, $intermediate['file'], $original_url );

		return array( $img_url, $width, $height, true );
	}

	/**
	 * Get attachment id
	 */
	private function get_attachment_id( $url, $acf_option = null ) {
		global $wpdb;
		if ( $acf_option != null ) {
			$attachment = $wpdb->get_col( $wpdb->prepare( "SELECT option_value FROM $wpdb->options WHERE option_name='%s';", 'options_' . $acf_option ) );
		}  else {
			$url_replace = str_replace(array('https://'), 'http://', $url);
			$attachment = $wpdb->get_col( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE guid like binary '%s';", $url_replace ) );
		}
		if ( empty( $attachment ) ) {
			$url_replace = str_replace(array('http://'), 'https://', $url);
			$attachment = $wpdb->get_col( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE guid like binary '%s';", $url_replace ) );
		}
		if ( empty( $attachment ) ) {
			$upload_dir = wp_upload_dir();
			$url_dir = $upload_dir['url'];
			$sub_dir = $upload_dir['subdir'];
			$pre_target = explode( $sub_dir, $url_dir );
			$pre2_target = explode($pre_target[0], $url);
			$target = explode( '/', $pre2_target[1] );
			if ( ! empty( $target[3] ) ) {
				$target     = $target[3];
				$attachment = $wpdb->get_col( "SELECT post_id FROM $wpdb->postmeta WHERE meta_key LIKE '_wp_attachment_metadata' AND meta_value LIKE  '%" . $target . "%'" );
			}
		}
		if ( ! empty( $attachment ) ) {
			return $attachment[0];
		} else {
			return false;
		}
	}

	/**
	 * Primary function cut image
	 *
	 * @return array|false
	 */
	function cut_image() {
		if ( ! $this->is_id ) {
			$image = $this->get_attachment_id( $this->image, $this->acf_option );
		}
		else {
			$image = $this->image;
		}
		$img = image_downsize( $image, $this->size );

		return $img;
	}

	/**
	 * Function get image url after cut
	 *
	 * @return string|false
	 */
	public function get_image_cut_url() {
		$img_url_arr = $this->cut_image();
		if ( ! empty( $img_url_arr ) ) {
			$img_url = $img_url_arr[0];

			return $img_url;
		} else {
			return false;
		}
	}
}