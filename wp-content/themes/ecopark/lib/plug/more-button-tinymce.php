<?php

/**
 * Class Cus_More_Tinymce
 */
class Cus_More_Tinymce {

	/**
	 * Admin Constructor
	 *
	 */
	public function __construct() {

		// Enable more button for default tinymce
		add_filter("mce_buttons_3", array( $this, 'enable_more_buttons' ));

	}

	/**
	 * Enable more buttons
	 *
	 * @access private
	 */
	function enable_more_buttons($buttons) {

		$buttons[] = 'fontselect';
		$buttons[] = 'fontsizeselect';
		$buttons[] = 'styleselect';
		$buttons[] = 'backcolor';
		$buttons[] = 'newdocument';
		$buttons[] = 'cut';
		$buttons[] = 'copy';
		$buttons[] = 'charmap';
		$buttons[] = 'hr';
		$buttons[] = 'visualaid';

		return $buttons;

	}

}

new Cus_More_Tinymce();