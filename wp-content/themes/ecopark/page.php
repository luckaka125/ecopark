<?php get_header(); ?>
<main class="default-page" id="main">
	<div class="content-default-block">
		<div class="row">
			<div class="column">
				<div class="wp-editor">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
</main>
<?php get_footer(); ?>
