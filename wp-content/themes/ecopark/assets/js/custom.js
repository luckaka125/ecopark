$('#locale-selector').on('change', function() {
    location.href = this.value;
});
var ajaxUrl = $('#theme_path').val()+'/project/frontend-ajax.php';
var load = true;
$(window).scroll(function() {
    if($(window).scrollTop() == $(document).height() - $(window).height()) {
        if($('body').hasClass('tax-product_cat')){
            if($('#page_num').data('total') >= $('#page_num').val() && load){
                load = false;
                var data={
                    action:'load_more_products',
                    page: $('#page_num').val(),
                    tax_id: $('#tax_id').val(),
                };
                $.post(ajaxUrl, data, function(result){
                    var json = result,
                        obj = JSON.parse(json);
                    $('#page_num').val(obj.page)
                    $(obj.data).insertBefore(".grid-footer.text-center");
                    load = true;
                });
            }
        }
    }
});

var wi=jQuery(document).width();
if(wi<=480){
    jQuery(".elementor-17 .elementor-element.elementor-element-7b3e53e .elementor-posts").owlCarousel({
        loop: false,
        responsiveClass: true,
        dots:true,
        nav:false,
        margin:10,
        items:1
  });
}







