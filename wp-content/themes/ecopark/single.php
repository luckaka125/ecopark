<?php get_header(); ?>
<main class="list-page page-default" id="main">
    <div class="content-default-block">
        <div class="row">
            <div class="column">
                <div class="banner">
                    <?php
                    $get_related_items = get_related_items(get_the_ID(), '', 4, 'post', '', '', '');
                    $banner  = get_field('page_banner');
                    if (!empty($banner)):
                        ?>
                        <img src="<?php echo $banner; ?>" alt="<?php the_title(); ?>">
                    <?php endif; ?>
                </div>

                </section>
                <section class="elementor-section elementor-top-section elementor-element elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-element_type="section">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-column elementor-col-100 elementor-top-column elementor-element" data-element_type="column">
                            <div class="elementor-widget-wrap elementor-element-populated">
                                <div class="elementor-element elementor-element-d067143 elementor-grid-2 elementor-grid-tablet-2 elementor-grid-mobile-1 elementor-posts--thumbnail-top elementor-card-shadow-yes elementor-posts__hover-gradient elementor-widget elementor-widget-posts" data-id="d067143" data-element_type="widget" data-settings="{&quot;cards_columns&quot;:&quot;2&quot;,&quot;cards_columns_tablet&quot;:&quot;2&quot;,&quot;cards_columns_mobile&quot;:&quot;1&quot;,&quot;cards_row_gap&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:35,&quot;sizes&quot;:[]}}" data-widget_type="posts.cards">
                                    <div class="elementor-widget-container" id="news-detail-page">
                                        <div class="wp-editor has_sidebar">
                                            <?php the_content(); ?>
                                            <?php
                                            if(!empty($get_related_items)):
                                                ?>
                                                <div class="related-news mobile">
                                                    <h3>TIN LIÊN QUAN</h3>
                                                    <div class="items-related-news">
                                                    <?php foreach ($get_related_items as $key => $post ):
                                                        setup_postdata($post);
                                                        if(!empty(has_excerpt())){
                                                            $content_intro         = get_the_excerpt();
                                                        }else{
                                                            $content_intro         = get_the_content();
                                                        }
                                                        $featured_img_url = get_the_post_thumbnail_url( get_the_ID() , 'large');
                                                        ?>
                                                        <div class="post-item">
                                                            <div class="post-img">
                                                                <a href="<?php the_permalink();?>">
                                                                    <img src="<?php echo $featured_img_url; ?>" alt="<?php the_title();?>">
                                                                </a>
                                                            </div>
                                                            <div class="post-content">
                                                                <a href="<?php the_permalink();?>"><?php the_title();?></a>
                                                                <p><?php echo excerpt_by_char(strip_tags($content_intro), 60, ' ...'); ?></p>
                                                            </div>
                                                        </div>
                                                    <?php endforeach;
                                                    wp_reset_postdata();
                                                    ?>
                                                </div>
                                              </div>
                                            <?php endif;?>
                                        </div>
                                        <div id="news-sidebar">
                                            <?php get_sidebar();?>
                                        </div>
                                        <?php
                                        if(!empty($get_related_items)):
                                            ?>
                                            <div class="related-news desktop">
                                                <h3>TIN LIÊN QUAN</h3>
                                                <div class="items-related-news">
                                                <?php foreach ($get_related_items as $key => $post ):
                                                    setup_postdata($post);
                                                    if(!empty(has_excerpt())){
                                                        $content_intro         = get_the_excerpt();
                                                    }else{
                                                        $content_intro         = get_the_content();
                                                    }
                                                    $featured_img_url = get_the_post_thumbnail_url( get_the_ID() , 'large');
                                                    ?>
                                                    <div class="post-item">
                                                        <div class="post-img">
                                                            <a href="<?php the_permalink();?>">
                                                                <img src="<?php echo $featured_img_url; ?>" alt="<?php the_title();?>">
                                                            </a>
                                                        </div>
                                                        <div class="post-content">
                                                            <a href="<?php the_permalink();?>"><?php the_title();?></a>
                                                            <p><?php echo excerpt_by_char(strip_tags($content_intro), 60, ' ...'); ?></p>
                                                        </div>
                                                    </div>
                                                <?php endforeach;
                                                wp_reset_postdata();
                                                ?>
                                                </div>
                                            </div>
                                        <?php endif;?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>
            </div>
        </div>
    </div>
</main>

<?php get_footer(); ?>
