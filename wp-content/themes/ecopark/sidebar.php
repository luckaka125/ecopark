<aside id="sidebar">
   

    <div class="">
            <h3 class="sidebar-head">SẢN PHẨM ĐANG BÁN</h3>
            <ul class="recent-posts round-images">
                <?php
                $args = array(
                    'post_type'      => 'page',
                    'posts_per_page' => 6,
                    'status' => 'public',
                    'orderby' => 'menu_order',
                    'order'   => 'ASC',
                    'cat' => 9
                );

                $products = new WP_Query( $args );
                if ( $products->have_posts() ) : ?>
                    <div class="product-selling-side">
                        <?php while ( $products->have_posts() ) : $products->the_post();
                            $featured_img_url = get_the_post_thumbnail_url( get_the_ID() , 'medium');
                            if(empty($featured_img_url)){
                                $featured_img_url = 'https://via.placeholder.com/500/?text=Ecopark';
                            }
                            ?>
                            <div class="product-side-item">
                                <a class="" href="<?php the_permalink(); ?>">
                                    <div class="elementor-fit-height"><img src="<?php echo $featured_img_url; ?>"></div>
                                </a>
                                <h3 class="elementor-post__title">
                                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                </h3>
                            </div>
                        <?php endwhile; ?>
                        <?php wp_reset_postdata(); ?>
                    </div>
                <?php endif; ?>
            </ul>
        </div>
    <div class="side-contact">
         <h3 class="sidebar-head">LIÊN HỆ</h3>
    <div class="form-contact">
        <?php echo do_shortcode('[gravityform id="4" title="false" description="false" ajax="true"]'); ?>
    </div>
    </div>
</aside>


