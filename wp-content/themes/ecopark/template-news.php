<?php
/* Template Name: News Template */
get_header();
?>
<main id="main" class="page-news-list">
    <div class="row">
        <div class="column">
            <div class="overview-wrapper">
                <div class="wp-editor">
                    <?php the_content();?>
                </div>
<!--                <section class="news-page elementor-section elementor-top-section elementor-element elementor-element-de08a6a elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-element_type="section">-->
<!--                <div class="elementor-container elementor-column-gap-default">-->
<!--                    <div class="elementor-top-column elementor-element" id="news-block">-->
<!--                    --><?php
//                    $num = 10;
//                    $page =  get_query_var('paged') ? get_query_var('paged') : 1;
//                    $news_lists = get_data_post('', $num, '', '', '', $page, '', false);
//                    $page_total = $news_lists->max_num_pages;
//                    if ( $news_lists->have_posts() ) :
//                        ?>
<!--                        --><?php
//                        $class = 'columns small-12 medium-6 large-4';
//                        $count= 1;
//                        while ( $news_lists->have_posts() ) {
//                            $news_lists->the_post();
//                            setup_postdata($post);
//                            if(!empty(has_excerpt())){
//                                $content_intro         = get_the_excerpt();
//                            }else{
//                                $content_intro         = get_the_content();
//                            }
//                            $featured_img_url = get_the_post_thumbnail_url( get_the_ID() , 'large');
//                            ?>
<!--                            <div class="new-item">-->
<!--                                --><?php //if($count%2 != 0):?>
<!--                                <div class="thumb">-->
<!--                                    <a href="--><?php //the_permalink(); ?><!--" title="--><?php //the_title(); ?><!--" class="image img-cover">-->
<!--                                        <img src="--><?php //echo $featured_img_url; ?><!--" alt="--><?php //the_title(); ?><!--">-->
<!--                                    </a>-->
<!--                                </div>-->
<!--                                <div class="news-content right">-->
<!--                                    <h3 class="title">-->
<!--                                        <a href="--><?php //the_permalink(); ?><!--" title="--><?php //the_title(); ?><!--">-->
<!--                                            --><?php //the_title(); ?>
<!--                                        </a>-->
<!--                                    </h3>-->
<!--                                    <div class="info">-->
<!--                                        <div class="description">--><?php //echo excerpt_by_char(strip_tags($content_intro), 200, ' ...'); ?><!--</div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                --><?php //else:?>
<!--                                    <div class="news-content left">-->
<!--                                        <h3 class="title">-->
<!--                                            <a href="--><?php //the_permalink(); ?><!--" title="--><?php //the_title(); ?><!--">-->
<!--                                                --><?php //the_title(); ?>
<!--                                            </a>-->
<!--                                        </h3>-->
<!--                                        <div class="info">-->
<!--                                            <div class="description">--><?php //echo excerpt_by_char(strip_tags($content_intro), 200, ' ...'); ?><!--</div>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                    <div class="thumb">-->
<!--                                        <a href="--><?php //the_permalink(); ?><!--" title="--><?php //the_title(); ?><!--" class="image img-cover">-->
<!--                                            <img src="--><?php //echo $featured_img_url; ?><!--" alt="--><?php //the_title(); ?><!--">-->
<!--                                        </a>-->
<!--                                    </div>-->
<!--                                --><?php //endif; ?>
<!--                            </div>-->
<!--                            --><?php
//                            $count++;
//                        }
//                        wp_reset_postdata();
//                        ?>
<!---->
<!--                    --><?php //endif; ?>
<!--                        --><?php //if($page_total > 1):?>
<!---->
<!--                            <div class="elementor-pagination">-->
<!--                                --><?php
//                                pagination(
//                                    array(
//                                        'total' => round($page_total, 0),
//                                        'prev_text' => ' <',
//                                        'next_text' => '> ',
//                                    )
//                                );
//                                ?>
<!--                            </div>-->
<!--                        --><?php //endif; ?>
<!--                </div>-->
<!---->
<!--                </div>-->
<!--                </section>-->
            </div>
        </div>
    </div>
</main>
<?php get_footer(); ?>