<?php

/**
 * Define variable
 */
if (!defined('THEME_URL')) {
    define('THEME_URL', get_template_directory_uri());
}
if (!defined('CHILD_THEME_URL')) {
    define('CHILD_THEME_URL', get_stylesheet_directory_uri());
}
//
///**
// * Include functions
// */
//require_once TEMPLATEPATH . '/lib/init/funcs.php';
//require_once TEMPLATEPATH . '/project/project.php';
//
//global $site_list, $devClass;
//
//require_once TEMPLATEPATH . '/project/DEV_DB.php';
//$devdb = new DEV_DB();
//global $devdb;
///**
// * Include config
// */
//require_once TEMPLATEPATH . '/config.php';
//
///**
// * Include theme-init
// */
//require_once TEMPLATEPATH . '/lib/init/theme-init.php';
//
///**
// * Include required
// */
foreach (glob(TEMPLATEPATH . '/project/inc/*.php') as $file_path) {
    get_require('/project/inc/' . basename($file_path));
}
function get_require( $template_name, $abs_path = '', $require_once = true ) {
    $located = '';

    if ( ! empty( $abs_path ) && file_exists( $abs_path . '/' . $template_name ) ) {
        $located = $abs_path . '/' . $template_name;
    } elseif ( file_exists( STYLESHEETPATH . '/' . $template_name ) ) {
        $located = STYLESHEETPATH . '/' . $template_name;
    } elseif ( file_exists( TEMPLATEPATH . '/' . $template_name ) ) {
        $located = TEMPLATEPATH . '/' . $template_name;
    }

    if ( '' != $located ) {
        global $posts, $post, $wp_did_header, $wp_query, $wp_rewrite, $wpdb, $wp_version, $wp, $id, $comment, $user_ID;

        if ( is_array( $wp_query->query_vars ) ) {
            extract( $wp_query->query_vars, EXTR_SKIP );
        }

        if ( isset( $s ) ) {
            $s = esc_attr( $s );
        }

        if ( $require_once ) {
            require_once( $located );
        } else {
            require( $located );
        }
    }


    return $located;
}

add_theme_support('automatic-feed-links');
add_theme_support('title-tag');
add_theme_support('post-thumbnails');
add_theme_support('woocommerce');

add_theme_support('html5', array(
    'search-form',
    'comment-form',
    'comment-list',
    'gallery',
    'caption',
));



function get_data_post( $post_id, $number = 10, $post_type = 'post', $taxonomy = '', $term_id = '', $page = '', $s = '', $flag = true, $random = false ) {
    $args = array(
        'posts_per_page'   => $number,
        'post_type'        => $post_type,
        'post_status'      => 'publish',
        'suppress_filters' => true
    );

    if($random) {
        $args['orderby'] = 'rand';
    } else {
        $args['orderby'] = array('menu_order' => 'ASC', 'date' => 'DESC');
    }
    if ( ! empty( $post_id ) && is_array($post_id) ) {
        $args['post__not_in'] = $post_id;
    } elseif(!empty($post_id)) {
        $args['exclude'] = $post_id;
    }

    if ( $page > 1 ) {
        $args['offset'] = ( $page - 1 ) * $number;
    }

    if ( ! empty( $s ) ) {
        $args['s'] = $s;
    }

    if($taxonomy == 'category') {
        $args['cat'] = $term_id;
    }

    if ( ! empty( $term_id ) && $taxonomy !='category' ) {
        if($flag) {
            $args['tax_query'] = array(
                array(
                    'taxonomy' => $taxonomy,
                    'field'    => 'term_id',
                    'terms'    => $term_id
                )
            );
        } else {
            $args['tax_query'] = array(
                array(
                    'taxonomy' => $taxonomy,
                    'field'    => 'term_id',
                    'terms'    => $term_id,
                    'operator' => 'IN',
                )
            );
        }
    }
    if ( ! empty( $author ) ) {
        $args['author'] = $author;
    }

    if ( $flag ) {
        return get_posts( $args );
    } else {
        return new WP_Query( $args );
    }
}


function pagination( $args = array() ) {
    $big = 999999999;
    global $wp_query;
    $args = wp_parse_args( $args, array(
        'base'      => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
        'format'    => '?paged=%#%',
        'current'   => max( 1, get_query_var( 'paged' ) ),
        'total'     => $wp_query->max_num_pages,
        'end_size'  => 1,
        'mid_size'  => 2,
        'prev_next' => true,
        'prev_text' => __( '« Previous' ),
        'next_text' => __( 'Next »' ),
    ) );
    echo paginate_links( $args );
}


/**
 * Function cut string by character
 * @param string $string
 * @param int $max_length
 * @param string $more_string
 * @return string $string
 **/
function excerpt_by_char($string, $max_length, $more_string = '...')
{
    $string = strip_shortcodes($string);
    if (mb_strlen($string, "UTF-8") > $max_length) {
        $max_length = $max_length - 3;
        $string = mb_substr($string, 0, $max_length, "UTF-8");
        $pos = strrpos($string, " ");
        if ($pos === false) {
            return substr($string, 0, $max_length) . $more_string;
        }
        return substr($string, 0, $pos) . $more_string;
    } else {
        return $string;
    }
}


function get_related_items($post_id, $arr, $num, $post_type = array(), $key, $taxonomy, $term_id, $lasted = true) {
    $list_select = array();
    if(!is_array_empty($arr)){
        foreach ($arr as $value){
            if(!empty($key)){
                if(!empty($value[$key])){
                    $list_select[] = $value[$key];
                }
            }else{
                $list_select = $arr;
            }
        }
    }
    if(count($list_select) < $num){
        $recent = get_data_post(array($post_id), $num, $post_type, $taxonomy, $term_id, 1, '', false);
        $recent = $recent->get_posts();
        $all_items = array_unique(array_merge($list_select,$recent), SORT_REGULAR  );
        if(count($all_items) < $num){
            if($lasted){
                $lasted = get_data_post(array($post_id), $num, $post_type, $taxonomy, '', 1, '', false);
                $lasted = $lasted->get_posts();
                $all_items = array_unique(array_merge($all_items, $lasted), SORT_REGULAR  );
            }
        }
    }else{
        $all_items = $list_select;
    }
    $all_items = array_slice($all_items, 0, $num);

    return $all_items;
}


/**
 * Convert to single array
 * @param array
 * @return array
 */
function convert_to_array($arr)  {
    $list = array();
    foreach( $arr as $value ) {
        // string, number ....
        if (is_scalar($value)) {
            $list[] = $value;
        } elseif (is_array($value)) {
            //array
            $list = array_merge($list,convert_to_array($value));
        }else{
            //object
            $list = array_merge($list,convert_to_array((array)$value));
        }
    }
    return $list;
}
/**
 * Check empty array
 * @param array
 * @return bool
 * True is empty
 */
function is_array_empty($array){
    $check = array();
    if(is_array($array)){
        $check = convert_to_array($array);
    }elseif(is_object($array)){
        $check = convert_to_array((array)$array);
    }else{
        if(!empty($array)){
            return false;
        }else{
            return true;
        }
    }
    if(!empty(array_filter($check))){
        return false;
    }else{
        return true;
    }
}

add_action( 'login_head', 'dev_custom_login_logo' );
function dev_custom_login_logo() {
    echo "<style type='text/css'>
                .login h1 a {
                    background-image: url('" . abs_js_link( 'assets/images/logo.png' ) . "');
                    background-size: 180px 100px;
                    width: 330px;
                    height: 100px;
                }
    </style>";
}
function abs_js_link( $src ) {
    return ( strpos( $src, 'http' ) === false ) ? THEME_URL . '/' . $src : $src;
}

add_filter( 'login_headerurl', 'dev_custom_login_logo_url' );
function dev_custom_login_logo_url( $url ) {
    return 'http://ecopark-land.com';
}