<?php
/* Template Name: List page Template */
get_header();
?>

    <main class="list-page" id="main">
        <div class="content-default-block">
            <div class="row">
                <div class="column">
                    <?php the_content(); ?>
        <section class="elementor-section elementor-top-section elementor-element elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-element_type="section">
            <div class="elementor-container elementor-column-gap-default">
                <div class="elementor-column elementor-col-100 elementor-top-column elementor-element" data-element_type="column">
                    <div class="elementor-widget-wrap elementor-element-populated">
                        <div class="elementor-element elementor-element-d067143 elementor-grid-4 elementor-grid-tablet-4 elementor-grid-mobile-1 elementor-posts--thumbnail-top elementor-card-shadow-yes elementor-posts__hover-gradient elementor-widget elementor-widget-posts" data-id="d067143" data-element_type="widget" data-settings="{&quot;cards_columns&quot;:&quot;4&quot;,&quot;cards_columns_tablet&quot;:&quot;2&quot;,&quot;cards_columns_mobile&quot;:&quot;1&quot;,&quot;cards_row_gap&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:35,&quot;sizes&quot;:[]}}" data-widget_type="posts.cards">
                            <div class="elementor-widget-container">
                                <div class="item-list-page">

                                    <?php
                                    $args = array(
                                        'post_type'      => 'page',
                                        'posts_per_page' => -1,
                                        'post_parent'    => $post->ID,
                                        'order'          => 'ASC',
                                        'orderby'        => 'menu_order'
                                    );

                                    $parent = new WP_Query( $args );
                                    if ( $parent->have_posts() ) : ?>
                                        <?php while ( $parent->have_posts() ) : $parent->the_post();
                                            if(!empty(has_excerpt())){
                                                $content_intro         = get_the_excerpt();
                                            }else{
                                                $content_intro         = get_the_content();
                                            }
                                            $featured_img_url = get_the_post_thumbnail_url( get_the_ID() , 'large');
                                            if(empty($featured_img_url)){
                                                $featured_img_url = 'https://via.placeholder.com/500/?text=Ecopark';
                                            }
                                            ?>
                                            <article class="elementor-post elementor-grid-item page type-page status-publish has-post-thumbnail hentry">
                                                <div class="elementor-post__card">
                                                    <a class="elementor-post__thumbnail__link" href="<?php the_permalink(); ?>">
                                                        <div class="elementor-fit-height"><img src="<?php echo $featured_img_url; ?>"></div>
                                                    </a>
                                                    <div class="content-page-list">
                                                        <h3 class="elementor-post__title">
                                                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                                        </h3>
                                                        <?php if(!empty($content_intro)):?>
                                                        <div class="elementor-post__excerpt" data-mh="excerpt">
                                                            <p><?php echo excerpt_by_char(strip_tags($content_intro), 120, ' ...'); ?></p>
                                                        </div>
                                                        <?php endif;?>
                                                        <a class="elementor-post__read-more" href="<?php the_permalink(); ?>">
                                                            Xem chi tiết			</a>
                                                    </div>
                                                </div>
                                            </article>
                                        <?php endwhile; ?>

                                    <?php endif; wp_reset_postdata(); ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
                </div>
            </div>
        </div>
    </main>
<?php get_footer(); ?>