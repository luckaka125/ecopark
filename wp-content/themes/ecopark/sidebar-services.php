<aside id="sidebar">
   

    <div class="">
            <h3 class="sidebar-head">THAM KHẢO THÊM</h3>
            <ul class="recent-posts round-images">
                <?php
                global $post;
                $args = array(
                    'post_type'      => 'page',
                    'posts_per_page' => 8,
                    'post__not_in' => array (get_the_ID()),
                    'post_parent'    => 1041,//$post->post_parent,
                    'order'          => 'rand()',
                );

                $items = new WP_Query( $args );
                if ( $items->have_posts() ) : ?>
                    <div class="product-selling-side">
                        <?php while ( $items->have_posts() ) : $items->the_post();
                            $featured_img_url = get_the_post_thumbnail_url( get_the_ID() , 'medium');
                            if(empty($featured_img_url)){
                                $featured_img_url = 'https://via.placeholder.com/500/?text=Ecopark';
                            }
                            ?>
                            <div class="product-side-item">
                                <a class="" href="<?php the_permalink(); ?>">
                                    <div class="elementor-fit-height"><img src="<?php echo $featured_img_url; ?>"></div>
                                </a>
                                <h3 class="elementor-post__title">
                                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                </h3>
                            </div>
                        <?php endwhile; ?>
                        <?php wp_reset_postdata(); ?>
                    </div>
                <?php endif; ?>
            </ul>
        </div>
</aside>


