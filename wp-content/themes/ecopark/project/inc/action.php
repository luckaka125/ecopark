<?php
function apartments_slide_sc( $atts = array() ) {
    extract(shortcode_atts(array(
        'number' => '5'
    ), $atts));
    $content = '';
    ob_start();
    set_query_var('number', $number);
    get_template_part('blocks/home', 'apartments');
    $content .= ob_get_clean();
    return $content;
}
add_shortcode('apartments_slide', 'apartments_slide_sc');


function apartments_slide_project( $atts = array() ) {
    extract(shortcode_atts(array(
        'number' => '5'
    ), $atts));
    $content = '';
    ob_start();
    get_template_part('blocks/apartments', 'project');
    $content .= ob_get_clean();
    return $content;
}
add_shortcode('apartments_slide_project', 'apartments_slide_project');


function infrastructure_slide_project( $atts = array() ) {
    extract(shortcode_atts(array(
        'number' => '5'
    ), $atts));
    $content = '';
    ob_start();
    get_template_part('blocks/infrastructure');
    $content .= ob_get_clean();
    return $content;
}
add_shortcode('infrastructure_project', 'infrastructure_slide_project');

function products_selling( $atts = array() ) {
    extract(shortcode_atts(array(
        'number' => '6'
    ), $atts));
    $content = '';
    ob_start();
    get_template_part('blocks/products_selling');
    $content .= ob_get_clean();
    return $content;
}
add_shortcode('products_selling', 'products_selling');

function products_transfer( $atts = array() ) {
    extract(shortcode_atts(array(
        'number' => '6'
    ), $atts));
    $content = '';
    ob_start();
    get_template_part('blocks/products_transfer');
    $content .= ob_get_clean();
    return $content;
}
add_shortcode('products_transfer', 'products_transfer');

function ecopark_related( $atts = array() ) {
    $content = '';
    ob_start();
    get_template_part('blocks/ecopark_related');
    $content .= ob_get_clean();
    return $content;
}
add_shortcode('ecopark_related', 'ecopark_related');



function categories_settings() {
    register_taxonomy_for_object_type('category', 'page');
}
add_action( 'init', 'categories_settings' );