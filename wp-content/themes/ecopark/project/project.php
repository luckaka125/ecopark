<?php
class Project extends DEV_Class {

	public static $instance;

	public static function getInstance()
	{
		if (null === static::$instance) {
			static::$instance = new static();
		}

		return static::$instance;
	}

	public function __construct() {
		$this->add_action('wp_head', 'add_favicon');
		$this->add_action('admin_head', 'add_favicon');
		$this->add_action('acf/init', 'add_acf_google_api');
		$this->add_action( 'init', 'unregister_tags' );
	 	$this->add_action('admin_menu', 'remove_comment_from_menu');
	 	$this->add_action('admin_bar_menu','remove_comment_from_admin_bar', 999);
	}
	function remove_comment_from_menu( $wp_admin_bar ) {
		remove_menu_page('edit-comments.php');
	}

	function remove_comment_from_admin_bar( $wp_admin_bar ) {
		$wp_admin_bar->remove_node('comments');
	}

	/** Get rid of tags on posts.*/
	function unregister_tags() {
		unregister_taxonomy_for_object_type( 'post_tag', 'post' );
	}

	// Move Yoast to bottom
	public function dev_yoast_to_bottom() {
		return 'low';
	}

	public function get_image_full($image) {
		if(empty($image)) {
			// get image in option
			$image = $this->get_option('opt_default_image');
		}
		if(empty($image)) {
			// get image default
			$image = wp_get_attachment_image_url($this->default_cut_image());
		}
		return $image;
	}

	/**
	 * set favicon to site
	 */
	public function add_favicon() {
		$favicon = $this->get_option('opt_general_fav');
		$favicon = $favicon['url'];
		if( !$favicon ) $favicon = THEME_URL.'/favicon.png';
		echo '<link rel="shortcut icon" href="'.$favicon.'" />';
	}

	/**
	 * add google api for acf google maps
	 */
	public function add_acf_google_api() {
		$google_key = $this->get_option('opt_google_key');
		if(!empty($google_key)) {
			acf_update_setting('google_api_key', $google_key);
		}
	}


	/*
	 * Get custom button
	 * */
	public function get_button( $data_button, $class = '', $type = 'link' ) {
		$button = @$data_button['button'][0];
		if ( empty( $button ) ) {
			$button = $data_button[0];
		}
		if ( ! empty( $button ) ) {
			$button_title = $button['button_title'];
			if ( ! empty( $button['button_page_link']) && $button['button_type'] == 'link'  ) {
				$button_link = $button['button_page_link'];
			} else {
				$button_link = $button['button_link'];
			}
			if ( $button['button_action'] == 'internal' ) {
				$target = '_self';
			} else {
				$target = '_blank';
			}
			if ( ! empty( $button_title ) && ! empty( $button_link ) ) {
				if ( $type == 'link' ) {
					return '<a class="' . $class . '" href="' . $button_link . '" target="' . $target . '"
					title="' . $button_title . '">' . $button_title . '</a>';
				} else {
					return '<button class="' . $class . '" href="' . $button_link . '" target="' . $target . '"
					title="' . $button_title . '">' . $button_title . '</button>';
				}
			} else {
				return '';
			}
		}
	}

	/*
	* get distance betwwen point
	*/

	function get_distance_between_points($latitude1, $longitude1, $latitude2, $longitude2) {
		$theta = $longitude1 - $longitude2;
		$miles = (sin(deg2rad($latitude1)) * sin(deg2rad($latitude2))) + (cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * cos(deg2rad($theta)));
		$miles = acos($miles);
		$miles = rad2deg($miles);
		$miles = $miles * 60 * 1.1515;
		$feet = $miles * 5280;
		$yards = $feet / 3;
		$kilometers = $miles * 1.609344;
		$meters = $kilometers * 1000;
		return array('miles'=> $miles ,'feet'=> $feet ,'yards' => $yards ,'kilometers' => $kilometers,'meters' => $meters );
	}

	/**
	* Add an action hook
	* @param $hook
	* @param $callback
	* @param $priority
	* @param $accepted_args
	*/
	public function add_action($hook, $callback, $priority = 10, $accepted_args = 1) {
		add_action($hook, array(
			$this,
			$callback
		) , $priority, $accepted_args);
	}
	 /**
	 * Add a filter hook
	 * @param $hook
	 * @param $callback
	 * @param $priority
	 * @param $accepted_args
	 */
	 public function add_filter($hook, $callback, $priority = 10, $accepted_args = 1) {
	 	add_filter($hook, array(
	 		$this,
	 		$callback
	 	) , $priority, $accepted_args);
	 }

	/**
	 * remove an action
	 * @param $hook
	 * @param $callback
	 */
	public function remove_action($hook, $callback) {
		remove_action($hook, array(
			$this,
			$callback
		));
	}

	/**
	 * Get related - recent - items
	 * @param $post_id
	 * @param $arr
	 * @param $num
	 * @param $post_type
	 * @param $key acf key repeater
	 * @param $taxonomy
	 * @param $term_id
	 * @param $lasted
	 */
	function get_related_items($post_id, $arr, $num, $post_type = array(), $key, $taxonomy, $term_id, $lasted = true) {
		global $devClass;
		global $devdb;
		$list_select = array();
		if(!$devClass->is_array_empty($arr)){
			foreach ($arr as $value){
				if(!empty($key)){
					if(!empty($value[$key])){
						$list_select[] = $value[$key];
					}
				}else{
					$list_select = $arr;
				}
			}
		}
		if(count($list_select) < $num){
			$recent = $devdb->get_data_post(array($post_id), $num, $post_type, $taxonomy, $term_id, 1, '', false);
			$recent = $recent->get_posts();
			$all_items = array_unique(array_merge($list_select,$recent), SORT_REGULAR  );
			if(count($all_items) < $num){
				if($lasted){
					$lasted = $recent = $devdb->get_data_post(array($post_id), $num, $post_type, $taxonomy, '', 1, '', false);
					$lasted = $lasted->get_posts();
					$all_items = array_unique(array_merge($all_items, $lasted), SORT_REGULAR  );
				}
			}
		}else{
			$all_items = $list_select;
		}
		$all_items = array_slice($all_items, 0, $num);

		return $all_items;
	}


	// Function cut string by character
	function excerpt_by_char($string, $max_length, $more_string = '...') {
		$string = strip_shortcodes($string);
		if (mb_strlen($string, "UTF-8") > $max_length) {
			$max_length = $max_length - 3;
			$string = mb_substr($string, 0, $max_length, "UTF-8");
			$pos = strrpos($string, " ");
			if ($pos === false) {
				return substr($string, 0, $max_length) . $more_string;
			}
			return substr($string, 0, $pos) . $more_string;
		} else {
			return $string;
		}
	}

    // Function cut string by character
    function viewed_products($product_id=0) {
	    if(!isset($_SESSION['product']) || empty($_SESSION['product'])){
            $_SESSION['product'] = array($product_id);
        }else{
	        $list = $_SESSION['product'];
            if(in_array($product_id, $list)){
                $k = array_search($product_id, $list);
                unset($list[$k]);
            }
            array_unshift($list, $product_id);
            $product_ids = array_slice($list, 0, 6);
            $_SESSION['product'] = $product_ids;
        }
    }

    function get_product_viewed() {
        $posts = array();
        if(isset( $_SESSION['product']) && !empty( $_SESSION['product'])){
            $args = array(
                'post_type'         => 'product',
                'post_status'       => 'publish',
                'orderby' => 'post__in',
                'posts_per_page'    => 6,
                'post__in'          => $_SESSION['product'],
            );
            $posts = get_posts($args);
        }
        return $posts;
    }

    function get_language_name($code=''){
        global $sitepress;
        $details = $sitepress->get_language_details($code);
        $language_name = $details['english_name'];
        return $language_name;
    }

}

if (!function_exists('Project')) {
	/**
	 * getinstance of DEV_Class class
	 *
	 * @param void
	 * @return void
	 *
	 */
	function Project()
	{
		return Project::getInstance();
	}
}

global $devClass;
$devClass = Project();