<?php
//mimic the actuall admin-ajax
define('DOING_AJAX', true);
if (!isset( $_POST['action']))
    die('-1');

/** Load WordPress Bootstrap */
require_once( $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php' );

//Typical headers
header('Content-Type: text/html');
send_nosniff_header();

//Disable caching
header('Cache-Control: no-cache');
header('Pragma: no-cache');

//$action = esc_attr(trim($_POST['action']));
$action =$_POST['action'];
if(is_user_logged_in())
    do_action('wp_ajax_'.$action);
else
    do_action('wp_ajax_nopriv_'.$action);
// Default status
wp_die( '0' );
