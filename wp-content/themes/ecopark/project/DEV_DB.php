<?php
class DEV_DB extends DEV_Class {
	function get_data_post( $post_id, $number = 10, $post_type = 'post', $taxonomy = '', $term_id = '', $page = '', $s = '', $flag = true, $random = false ) {
		$args = array(
			'posts_per_page'   => $number,
			'post_type'        => $post_type,
			'post_status'      => 'publish',
			'suppress_filters' => true
		);

		if($random) {
			$args['orderby'] = 'rand';
		} else {
			$args['orderby'] = array('menu_order' => 'ASC', 'date' => 'DESC');
		}
		if ( ! empty( $post_id ) && is_array($post_id) ) {
			$args['post__not_in'] = $post_id;
		} elseif(!empty($post_id)) {
			$args['exclude'] = $post_id;
		}

		if ( $page > 1 ) {
			$args['offset'] = ( $page - 1 ) * $number;
		}

		if ( ! empty( $s ) ) {
			$args['s'] = $s;
		}

		if($taxonomy == 'category') {
			$args['cat'] = $term_id;
		}

		if ( ! empty( $term_id ) && $taxonomy !='category' ) {
			if($flag) {
				$args['tax_query'] = array(
					array(
						'taxonomy' => $taxonomy,
						'field'    => 'term_id',
						'terms'    => $term_id
					)
				);
			} else {
				$args['tax_query'] = array(
					array(
						'taxonomy' => $taxonomy,
						'field'    => 'term_id',
						'terms'    => $term_id,
						'operator' => 'IN',
					)
				);
			}
		}
		if ( ! empty( $author ) ) {
			$args['author'] = $author;
		}
		
		if ( $flag ) {
			return get_posts( $args );
		} else {
			return new WP_Query( $args );
		}
	}	
}