<?php
$infrastructure = get_field('infrastructure_items', get_the_ID());

if(!empty($infrastructure)):
    ?>
    <div class="elementor-column-gap-default">
        <?php foreach ($infrastructure as $key => $item): ?>
        <div class="elementor-column elementor-col-25 infrastructure-item modal__trigger" data-modal="#modal<?php echo $key;?>">
            <div class="icon"><img src="<?php echo $item['icon'];?>" alt="<?php echo $item['title'];?>"></div>
            <div class="title-infrastructure"><?php echo $item['title'];?></div>
        </div>
        <?php endforeach;?>
    </div>
    <?php foreach ($infrastructure as $key => $item): ?>

        <div id="modal<?php echo $key;?>" class="modal modal__bg" role="dialog" aria-hidden="true">
            <div class="modal__dialog">
                <div class="modal__content">
                    <div class="icon"><img src="<?php echo $item['icon'];?>" alt="<?php echo $item['title'];?>"></div>
                    <h1><?php echo $item['title'];?></h1>
                    <?php echo $item['text'];?>
                    <a href="" class="modal__close demo-close">
                    </a>
                </div>
            </div>
        </div>
<?php endforeach;?>
<?php endif; ?>

