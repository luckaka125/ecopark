<?php  
$actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$img_cut_url = '';
global $devClass;
if (has_post_thumbnail()){
$featured_img_url = get_the_post_thumbnail_url( get_the_ID() );
$img_cut_url      = $devClass->get_image_cut_url( $featured_img_url, array( 500, 300 ) );
}
?>
<a href="//www.facebook.com/sharer.php?u=<?php echo $actual_link; ?>" data-label="Facebook" onclick="window.open(this.href,this.title,'width=500,height=500,top=300px,left=300px');  return false;" rel="nofollow" target="_blank" class="icon primary button circle tooltip facebook tooltipstered"><i class="icon-facebook"></i></a>
<a href="//twitter.com/share?url=<?php echo $actual_link; ?>" onclick="window.open(this.href,this.title,'width=500,height=500,top=300px,left=300px');  return false;" rel="nofollow" target="_blank" class="icon primary button circle tooltip twitter tooltipstered"><i class="icon-twitter"></i></a>
<a href="//pinterest.com/pin/create/button/?url=<?php echo $actual_link; ?>/&amp;media=<?php echo $img_cut_url; ?>" onclick="window.open(this.href,this.title,'width=500,height=500,top=300px,left=300px');  return false;" rel="nofollow" target="_blank" class="icon primary button circle tooltip pinterest tooltipstered"><i class="icon-pinterest"></i></a>
<a href="//plus.google.com/share?url=<?php echo $actual_link; ?>" target="_blank" class="icon primary button circle tooltip google-plus tooltipstered" onclick="window.open(this.href,this.title,'width=500,height=500,top=300px,left=300px');  return false;" rel="nofollow"><i class="icon-google-plus"></i></a>
<a href="//www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo $actual_link; ?>/&amp;title=<?php the_title(); ?>" onclick="window.open(this.href,this.title,'width=500,height=500,top=300px,left=300px');  return false;" rel="nofollow" target="_blank" class="icon primary button circle tooltip linkedin tooltipstered"><i class="icon-linkedin"></i></a>
