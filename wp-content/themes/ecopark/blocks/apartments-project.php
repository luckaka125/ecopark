<?php
$apartments = get_field('apartments_plans_items', get_the_ID());
if(!empty($apartments)):
    ?>
    <div class="apartment-slide">
        <section class="slider">
            <?php foreach ($apartments as $key => $item):
                $ap_properties = $item['content'];
                ?>
                <div>
                    <div class="apartment-data">
                        <?php if(!empty($ap_properties)):?>
                            <ul class="apartment-properties">
                                <?php foreach ($ap_properties as $key => $value):

                                    ?>
                                    <li>
                                        <span><?php echo $value['name'];?></span>
                                        <em><?php echo $value['value'];?></em>
                                    </li>
                                <?php endforeach;?>
                            </ul>
                        <?php endif; ?>
                    </div>
                    <div class="apartment-img">
                        <img src="<?php echo $item['image']; ?>" alt="<?php echo $item['title']; ?>">
                    </div>
                </div>
            <?php endforeach;?>

        </section>
        <div class="slider_dots">
            <?php foreach ($apartments as $key => $item): ?>
                <div class="slider_navigators"><?php echo $item['title']; ?></div>
            <?php endforeach;?>
        </div>
    </div>
    <style type="text/css">
        .apartment-slide{position: relative;}
        .slider {
            width: auto;
            margin: 60px 50px 50px;
        }

        .slick-slide {
            padding: 20px 0;
            font-size: 18px;
            font-family: "Arial", "Helvetica";
            text-align: center;
        }
        .slick-slide img {
            display: inline-block;
        }

        .slick-prev:before,
        .slick-next:before {
            color: black;
        }

        .slick-dots li {
            width: auto;
        }

        .pager__item {
            padding: 0 12px;
            display: block;
        }



        .slick-initialized .slick-slide {
            display: block;
            border: none;
            outline: none;
        }

        .slider_dots  {
            top: -75px;
            left: 0;
            right: 0;
            position: absolute;
            border-bottom: 1px solid #e8ebf2;
        }

        .slider_dots.slick-initialized .slick-current.slick-active{

            border-bottom: 4px solid #E5DF24;
        }
        .apartment-data, .apartment-img{
            width: 50%;
            float: left;
            margin-top: 25px;
        }
        ul.apartment-properties {
            list-style: none;
            margin: 0;
            padding: 0;
        }
        ul.apartment-properties li {
            text-align: left;
            padding-top: 8px;
            margin-top: 8px;
            border-top: 1px solid #ddd;
            color: #8A8D98;
            font-size: 16px;
        }
        ul.apartment-properties li em{float: right;color: #20252E;}
        .black-button{
            border-radius: 35px;
            color: #FFFFFF;
            background-color: #20252E;
            padding: 20px 35px;
            float: left;
            margin-top: 30px;
        }

    </style>
<?php endif; ?>