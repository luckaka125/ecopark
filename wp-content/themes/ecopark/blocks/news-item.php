<?php
global $devClass;
if(!empty(has_excerpt())){
    $content_intro         = get_the_excerpt();
}else{
    $content_intro         = get_the_content();
}
$main_cate =  $devClass->get_main_category(get_the_ID(), 'category');
$featured_img_url = get_the_post_thumbnail_url( get_the_ID() );
if(empty($featured_img_url)){
    $featured_img_url = $devClass->get_option('opt_feature_image_default');
}
$img_cut_url      = $devClass->get_image_cut_url( $featured_img_url, array( 449, 290 ) );
?>

<li>
    <div class="catalog">
        <div class="thumb">
          <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="image img-cover">
            <img src="<?php echo $img_cut_url; ?>" alt="<?php the_title(); ?>">
          </a>
        </div>
        <h3 class="title">
          <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
            <?php the_title(); ?>
          </a>
        </h3>
        <div class="info">
            <div class="description"><?php echo $devClass->excerpt_by_char(strip_tags($content_intro), 200, ' ...'); ?></div>
            <div class="readmore">
              <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="uk-button btn-readmore">Xem chi tiết</a>
            </div>
        </div>
    </div>
</li>
