
   


<?php
                if($post->post_parent==1041){
                    $p=$post->ID;
                }else{
                    $p=$post->post_parent;
                }
                $args = array(
                    'post_type'      => 'page',
                    'posts_per_page' => -1,
                    'post_parent'    => $p,
                    'order'          => 'ASC',
                    'orderby'        => 'menu_order'
                );

                $parent = new WP_Query( $args );
                if ( $parent->have_posts() ) : ?>

    <div class="list-right sidebar ">
        <div class="article-list ">
        <h3 class="sidebar-head">BÀI VIẾT</h3>
        <div class="slide-list-mobile">
                
                    <?php while ( $parent->have_posts() ) : $parent->the_post();
                        if(!empty(has_excerpt())){
                            $content_intro         = get_the_excerpt();
                        }else{
                            $content_intro         = get_the_content();
                        }
                        $featured_img_url = get_the_post_thumbnail_url( get_the_ID() , 'large');
                        if(empty($featured_img_url)){
                            $featured_img_url = 'https://via.placeholder.com/500/?text=Ecopark';
                        }
                        ?>
                        <article class="elementor-post elementor-grid-item page type-page status-publish has-post-thumbnail hentry">
                            <div class="elementor-post__card">
                                <a href="<?php the_permalink(); ?>">
                                    <div class="elementor-fit-height"><img src="<?php echo $featured_img_url; ?>"></div>
                                </a>
                                <div class="content-page-list">
                                    <h3 class="elementor-post__title">
                                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                    </h3>
                                    <?php if(!empty($content_intro)):?>
                                            <div class="elementor-post__excerpt">
                                                <p><?php echo excerpt_by_char(strip_tags($content_intro), 80, ' ...'); ?></p>
                                            </div>
                                    <?php endif;?>
                                </div>
                            </div>
                        </article>
                    <?php endwhile; ?>

               

         </div>
     </div>
     </div>
 <?php endif; wp_reset_postdata(); ?>
     

    