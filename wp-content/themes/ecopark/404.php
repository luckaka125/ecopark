<?php get_header();
global $devClass;
?>
<main id="main" class="page-content clearfix">
    <a href="#" id="main-content" tabindex="-1"></a>
    <div class="row error-page-wrapper collapse" style="background-image:url('/on/demandware.static/-/Library-Sites-BerlutiSharedLibrary/default/dw2cb9732d/error-message-visual3.jpg');">
        <div class="error-page-container small-24 columns align-middle">
            <img data-interchange="
                [https://www.berluti.com/dw/image/v2/BBMR_PRD/on/demandware.static/-/Library-Sites-BerlutiSharedLibrary/default/dw2cb9732d/error-message-visual3.jpg, small], [https://www.berluti.com/dw/image/v2/BBMR_PRD/on/demandware.static/-/Library-Sites-BerlutiSharedLibrary/default/dw2cb9732d/error-message-visual3.jpg, medium], [https://www.berluti.com/dw/image/v2/BBMR_PRD/on/demandware.static/-/Library-Sites-BerlutiSharedLibrary/default/dw2cb9732d/error-message-visual3.jpg, large];
                " src="https://www.berluti.com/dw/image/v2/BBMR_PRD/on/demandware.static/-/Library-Sites-BerlutiSharedLibrary/default/dw2cb9732d/error-message-visual3.jpg" alt="">
            <div class="row align-center error-text">
                <div class="small-24 large-12 columns text-center">
                    <div>
                        <p class="error-headline">Sorry, the page can not be found</p>
                        <p class="error-subtitle">We apologize for the inconvenience and invite you to continue experiencing our website</p>
                    </div>
                    <a href="/en-int/homepage/" class="button btn-large btn-tall">Back to website</a>
                </div>
            </div>
        </div>
    </div>
</main>
<?php get_footer(); ?>
